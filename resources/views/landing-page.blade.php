@extends('layout')

@section('title', 'Landing Page')

@section('content')

        @foreach($products as $product)
            <a href="{{ route('products.show', $product->slug ) }}">
                name: {{ $product->name }} <br>
                description: {{ $product->description }} <br>                
                price: {{ $product->presentPrice() }} <br>
            </a>
            <hr>

        @endforeach


@endsection
