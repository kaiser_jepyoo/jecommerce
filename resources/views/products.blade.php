@extends('layout')

@section('title', 'Products')

@section('content')

    <h3>Products: </h3>

    @foreach($products as $product)
        <a href="{{ route('products.show', $product->slug ) }}">
            name: {{ $product->name }} <br>
            description: {{ $product->description }} <br>                
            price: {{ $product->presentPrice() }} <br>
        </a>
        <hr>
    @endforeach

@endsection