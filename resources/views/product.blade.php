@extends('layout')

@section('title', 'Product')

@section('content')

        <h3>Product: </h3>


        name: {{ $product->name }} <br>
        description: {{ $product->description }} <br>                
        price: {{ $product->presentPrice() }} <br>            
        <br>

        <a href="#">Add to Cart</a>
        <hr>

        @include('partials.might-like')

@endsection


