<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title> @yield('title') </title>

        </style>
    </head>
    <body>
        <ul>
            <li><a href="{{ route('landing-page') }}">Home</a></li>
            <li><a href="{{ route('products.index') }}">Products</a></li>
            <li><a href="{{ route('cart.index') }}">Cart</a></li>

        </ul>

        @yield('content')

    </body>
</html>
