<h3>YOU MIGHT ALSO LIKE:</h3>

@foreach($mightAlsoLike as $product)

    <a href="{{ route('products.show', $product->slug ) }}">
        name: {{ $product->name }} <br>
        description: {{ $product->description }} <br>                
        price: {{ $product->presentPrice() }} <br>
    </a>
    <hr>

@endforeach